from django.contrib import admin

from .models import Comentario, Contenido

# Register your models here.

admin.site.register(Contenido)
admin.site.register(Comentario)
