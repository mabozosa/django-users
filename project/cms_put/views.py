from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.contrib.auth import logout

from .models import Contenido, Comentario


# Create your views here.

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":  # metodo PUT
        # Guarda el cuerpo de la peticion HTTP (PUT)
        valor = request.body.decode('utf-8')
    elif request.method == "POST":  # Metodo POST
        action = request.POST['action']
        if action == "Enviar Contenido":
            # Guarda el campo valor del formulario
            valor = request.POST['valor']
    if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
        if request.user.is_authenticated:
            # Solo el usuario autenticado puede hacer modificaciones
            try:  # Actualiza el contenido para una clave ya existente
                c = Contenido.objects.get(clave=llave)
                c.valor = valor
            # Se crea nuevo contenido si no existe la clave
            except Contenido.DoesNotExist:
                # Nuevo objeto contenido con su clave y valor
                c = Contenido(clave=llave, valor=valor)
            c.save()  # Guarda el contenido en la base de datos

    if request.method == "POST" and action == "Enviar Comentario":
        c = get_object_or_404(Contenido, clave=llave)
        titulo = request.POST['titulo']
        cuerpo = request.POST['cuerpo']
        q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())
        q.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    autenticado = request.user.is_authenticated
    usuario = request.user.username
    context = {'contenido': contenido, 'autenticado': autenticado, 'user': usuario}
    return render(request, 'cmsusersput/content.html', context)


def index(request):
    content_list = Contenido.objects.all()
    autenticado = request.user.is_authenticated
    context = {'content_list': content_list, 'autenticado': autenticado}
    return render(request, 'cmsusersput/index.html', context)


def loggedIn(request):
    if request.user.is_authenticated:
        logged = "Logged in as " + request.user.username + ". " +\
                 "<br><br><a href='/cms'>Volver al inicio.</a>"
    else:
        logged = "Not logged in. <a href='/login'>Login</a>"
    return HttpResponse(logged)


def logout_view(request):
    logout(request)
    return redirect("/cms/")


def iniciar_sesion(request):
    return redirect("/login")
